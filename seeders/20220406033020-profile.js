'use strict';
const profileData = require('../masterdata/profile.json')

module.exports = {
  async up (queryInterface, Sequelize) {
    const profile = profileData.map((eachProfile) => {
      eachProfile.createdAt = new Date();
      eachProfile.updatedAt = new Date();
      return eachProfile;
    });
   await queryInterface.bulkInsert('Profiles', profile);
  },

  async down (queryInterface, Sequelize) {
     await queryInterface.bulkDelete('Profiles', null, { truncate: true, restartIdentity: true });
  }
};
