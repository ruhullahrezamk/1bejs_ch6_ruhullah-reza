'use strict';
const postData = require('../masterdata/post.json')

module.exports = {
  async up (queryInterface, Sequelize) {
    const post = postData.map((eachPost) => {
      eachPost.createdAt = new Date();
      eachPost.updatedAt = new Date();
      return eachPost;
    });

   await queryInterface.bulkInsert('Posts', post);
  },

  async down (queryInterface, Sequelize) {

    await queryInterface.bulkDelete('Posts', null, { truncate: true, restartIdentity: true });
  }
};
