'use strict';
const accountData = require('../masterdata/account.json')
const bcrypt = require('bcryptjs')
require('dotenv').config()

module.exports = {
  async up (queryInterface, Sequelize) {
    const salt = await bcrypt.genSalt(+process.env.salt_round)
    const account = accountData.map((eachAccount) => {
      eachAccount.password  = bcrypt.hashSync(eachAccount.password,salt)
      eachAccount.createdAt = new Date();
      eachAccount.updatedAt = new Date();
      return eachAccount;
    });

   await queryInterface.bulkInsert('Accounts', account);
  },

  async down (queryInterface, Sequelize) {

     await queryInterface.bulkDelete('Accounts', null, { truncate: true, restartIdentity: true }) 
  }
};
