'use strict';
const commentData = require('../masterdata/comment.json')

module.exports = {
  async up (queryInterface, Sequelize) {
    const comment = commentData.map((eachComment) => {
      eachComment.createdAt = new Date();
      eachComment.updatedAt = new Date();
      return eachComment;
    });

   await queryInterface.bulkInsert('Comments', comment);
  },

  async down (queryInterface, Sequelize) {

    await queryInterface.bulkDelete('Comments', null, { truncate: true, restartIdentity: true });
  }
};
