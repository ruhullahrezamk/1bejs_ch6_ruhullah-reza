const jwt = require('jsonwebtoken')
const Response = require('./Response')
require('dotenv').config()

module.exports = function(req,res,next){
    const response = new Response(res)

    const auth = req.header('Authorization')
    if(!auth) return response.Fail(response.Unauthorized,'Authentication Needed')

    const token = auth.split(' ')[1]

    try{
        const verified = jwt.verify(token, process.env.jwt_secret)
        req.user = verified
        next()
    }catch(err){
        const errorList =['JsonWebTokenError','TokenExpiredError']
        errorList.includes(err.name)? response.Fail(response.BadRequest,err.message)
                                    : response.Fail(response.InternalServerError,'internal server error')
    }
}