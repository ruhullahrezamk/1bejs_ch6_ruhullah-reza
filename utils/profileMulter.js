const fs = require('fs')
const path = require('path')
const multer = require('multer')

const storage = multer.diskStorage({
  destination: function (req, file, cb) {
    const path = './public/img/profile'
    if (!fs.existsSync(path)) fs.mkdirSync(path, { recursive: true })
    cb(null, path)
  },
  filename: function (req, file, cb) {
    const extension = path.extname(file.originalname)
    cb(null, file.fieldname + '-' + Date.now() + extension)
  }
})

const uploadProfile = multer({ 
  storage: storage,
  limits: { fileSize: 5e+7 },
  fileFilter: (req, file, cb) => {   
    const validImageExtension = ['image/png','image/jpeg','image/jpg']
    if (validImageExtension.includes(file.mimetype)) cb(null, true);
    else cb("invalid file extension", false)
  } 
})

module.exports = uploadProfile