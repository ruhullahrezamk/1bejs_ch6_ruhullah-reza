const Response = require('./Response')
require('dotenv').config()

module.exports = function(req,res,next){
    const response = new Response(res)
    const isAdmin = req.user.isAdmin
    if(!isAdmin) return response.Fail(response.Forbidden,'anda tidak punya hak')
    next()
}