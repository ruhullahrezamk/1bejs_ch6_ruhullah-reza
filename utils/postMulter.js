const fs = require('fs')
const path = require('path')
const multer = require('multer')

const storage = multer.diskStorage({
  destination: function (req, file, cb) {
    const pathImage = './public/img/post'
    const pathVideo = './public/video'

    if (!fs.existsSync(pathImage)) fs.mkdirSync(pathImage, { recursive: true })
    if (!fs.existsSync(pathVideo)) fs.mkdirSync(pathVideo, { recursive: true })

    const validVideoExtension = ['video/mp4','video/x-matroska']
    const validImageExtension = ['image/png','image/jpeg','image/jpg']

    if(validVideoExtension.includes(file.mimetype)) cb(null,pathVideo);
    else if(validImageExtension.includes(file.mimetype)) cb(null,pathImage);
    else cb("invalid file extension",false)
  },

  filename: function (req, file, cb) {
    const extension = path.extname(file.originalname)   
    cb(null, req.user.username+ '-' + Date.now() + extension)
  }

})

const uploadMedia = multer({ 
  storage: storage,
  limits: { fileSize: 10e+7 }
})

module.exports = uploadMedia