const cloudinary = require('cloudinary').v2;
const Response = require('../utils/Response')
require('dotenv').config()

cloudinary.config({ 
    cloud_name: process.env.cloud_name, 
    api_key: +process.env.api_key, 
    api_secret: process.env.api_secret,
    secure: true
});


const postUpload = async (req, res, next) => {
    const response = new Response(res)
    try {
        if(req.file){
            const media_type = req.file.mimetype.split('/')[0]
            const foldering = `social-media/post/${media_type}`;
            
            const options = { 
                resource_type : "auto",
                folder: foldering
            }    

            const uploadedMedia = await cloudinary.uploader.upload(req.file.path,options);
            req.body.cloud_media_url = uploadedMedia.secure_url;
        }   
        next();
    } catch (error) {
        return response.Fail(error.http_code,error,error.message.message)
    }
};

const profileUpload = async (req, res, next) => {
    const response = new Response(res)
    try {
        if(req.file){         
            const media_type = req.file.mimetype.split('/')[0]
            const foldering = `social-media/profile/${media_type}`;
            const uploadedMedia = await cloudinary.uploader.upload(req.file.path, {
                folder: foldering
            });
            req.body.cloud_media_url = uploadedMedia.secure_url;
        }   
        next();
    } catch (error) {
        return response.Fail(error.http_code,error,error.message.message)
    }
};

const deleteImage = async (img_url) => {
    const img_name = img_url.split('/').pop()
    const folder = img_url.split('/')[8]
    const img_folder = img_url.split('/')[9]
    const img_id = img_name.split('.')[0]
    await cloudinary.uploader.destroy(`social-media/${folder}/${img_folder}/${img_id}`, result => result)
}

module.exports = {postUpload,profileUpload,deleteImage};