const nodemailer = require('nodemailer');
require('dotenv').config()

const sendEmail = (receiver,subject,messageToBeSent) => {

    const mailOptions = {
        from: process.env.email,
        to: `${receiver}`,
        subject: `${subject}`,
        text: `${messageToBeSent}`
    };

    const transporter = nodemailer.createTransport({
        host: process.env.host,
        port: 587,
        auth: {
            user: process.env.email,
            pass: process.env.emailAuth
        }
    });

    return new Promise((resolve, reject) => {
        transporter.sendMail(mailOptions, (err, info) => {
            if (err) reject(err);
            else resolve(info);
        });
    });
};

module.exports = sendEmail

