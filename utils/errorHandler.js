const Response = require('../utils/Response')
module.exports = (err, req, res, next) => {
    const response = new Response(res)
    
    const invalidType = err === "invalid file extension"
    const tooLarge = err.message === "File too large"
    const invalidJson = err.type === "entity.parse.failed"

    if(invalidType || tooLarge || invalidJson) return response.Fail(response.BadRequest,err.message||err)
    
    else return response.Fail(response.InternalServerError,'internal server error')  
}