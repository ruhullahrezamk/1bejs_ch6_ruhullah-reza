const {Account, Post, Comment, Profile} = require('../models');
const { deleteImage } = require("../utils/cloudinary");
const Response = require('../utils/Response')
const fs = require('fs')

module.exports ={

    createPost : async (req,res)=>{
        const {title,content,cloud_media_url} = req.body
        const user = req.user
        const response = new Response(res)
        const media = req.file
        try {

            const postData = {accountId:user.id,title,content}
            
            if(media) {
                postData.media_url = cloud_media_url
                postData.media_path = media.path
            }
          
            const newPost = await Post.create(postData)

            if(newPost) {
                delete newPost.dataValues.createdAt;
                delete newPost.dataValues.updatedAt;
                delete newPost.dataValues.accountId;

                response.Success(response.Created,newPost)
            }    

        } catch (error) {
            response.Fail(response.InternalServerError,'Internal Server Error')
        }
    },

    getAllPost : async (req,res)=>{
        const response = new Response(res)  
        try {   
            const options = {
                include : [{
                    model:Comment,
                    attributes:['id','message'],
                    include: [{
                        model:Account,
                        attributes:['username'],
                        include: [{
                            model:Profile,
                            attributes:['nickname']
                        }], 
                    }]
                },{
                    model:Account,
                    as:'owner',
                    attributes:['username']
                }],
                attributes: ['id', 'title','content','media_url'],    
            };

            let { page, row } = req.query;

            if(page) options.offset = page - 1
            if(row) options.limit = row

            const allPost = await Post.findAll(options)

            const allPostMapped = allPost.map(eachPost=>{
                const ownerName = eachPost.dataValues.owner.dataValues.username
                delete eachPost.dataValues.owner
                eachPost.dataValues.owner = ownerName
                eachPost.Comments = eachPost.dataValues.Comments.forEach(eachComment=>{
                    eachComment.dataValues.nickname = eachComment.dataValues.Account.dataValues.Profile.dataValues.nickname
                    eachComment.dataValues.username = eachComment.dataValues.Account.dataValues.username
                    delete eachComment.dataValues.Account
                    return eachComment
                })
                return eachPost
            })

            return response.Success(response.Ok,allPostMapped) 

        } catch (error) {
            response.Fail(response.InternalServerError,'Internal Server Error')
        }
    },

    getAllPostByUsername : async (req,res)=>{
        const username = req.params.username
        const response = new Response(res)
        try {   
            const owner = await Account.findOne({
                where:{username}
            })
            
            if(!owner) return response.Fail(response.NotFound,'user tidak ditemukan')

            const options = {
                include : [{
                    model:Account,
                    as:'owner',
                    where:{
                        username
                    },
                    attributes:[]
                }],
                attributes: ['id', 'title','content','media_url'],     
            };

            let { page, row } = req.query;

            if(page) options.offset = page - 1
            if(row) options.limit = row

            const allPost = await Post.findAll(options);

            return response.Success(response.Ok,allPost) 

        } catch (error) {
            response.Fail(response.InternalServerError,'Internal Server Error')
        }
    },

    getPostByid : async (req,res)=>{
        const id = req.params.id
        const response = new Response(res)
        try {
            const options = {
                include : [{
                    model:Comment,
                    attributes:['id','message'],
                    include: [{
                        model:Account,
                        attributes:['username'],
                        include: [{
                            model:Profile,
                            attributes:['nickname']
                        }]
                    }]
                },{
                    model:Account,
                    as:'owner',
                    attributes:['username'],
                    include: [{
                            model:Profile,
                            attributes:['nickname','description','img_url']
                        }]
                }],
                attributes: ['id', 'title','content','media_url'],
                where:{id}  
            }

            const post = await Post.findOne(options)
            if(!post) return response.Fail(response.NotFound,`post dengan id:${id} tidak ditemukan`)

            post.Comments = post.dataValues.Comments.forEach(eachComment=>{
                eachComment.dataValues.nickname = eachComment.dataValues.Account.dataValues.Profile.dataValues.nickname
                eachComment.dataValues.username = eachComment.dataValues.Account.dataValues.username
                delete eachComment.dataValues.Account
                return eachComment
            })

            response.Success(response.Ok,post)

        } catch (error) {
            response.Fail(response.InternalServerError,'Internal Server Error')
        }
    },

    updatePostById : async (req,res)=>{
        const {title,content,cloud_media_url} = req.body
        const user = req.user
        const response = new Response(res)
        const media = req.file
        try {
            const id = req.params.id

            const findPost = await Post.findOne({where:{id}})
            if(!findPost) return response.Fail(response.NotFound,`post dengan id:${id} tidak ditemukan`)

            const validUser = findPost.accountId === user.id
            const isAdmin = user.isAdmin
            if(!validUser && !isAdmin) return response.Fail(response.Forbidden,`anda tidak punya hak untuk update post dengan id:${id}`)

            const postData = {title,content}
            if(media) {
                postData.media_url = cloud_media_url
                postData.media_path = media.path
            }
            
            if(findPost.media_url){
                await deleteImage(findPost.media_url)
            }

            const currentMediaPath = findPost.media_path
            if(currentMediaPath && fs.existsSync(currentMediaPath)){
                fs.unlinkSync(currentMediaPath)
            } 

            const updatedPost = await Post.update(postData,{where:{ id }})

            updatedPost[0] ? response.Success(response.Ok,`post dengan id:${id} berhasil diupdate`)
                           : response.Fail(response.NotFound,`post dengan id:${id} tidak ditemukan`)
 
        } catch (error) {
            response.Fail(response.InternalServerError,'Internal Server Error')
        }
    },

    deletePostById : async (req,res)=>{
        const id = req.params.id;
        const user = req.user
        const response = new Response(res)
        try { 
            const findPost = await Post.findOne({where:{id}})
            if(!findPost) return response.Fail(response.NotFound,`post dengan id:${id} tidak ditemukan`)

            const validUser = findPost.accountId === user.id
            const isAdmin = user.isAdmin
            if(!validUser && !isAdmin) return response.Fail(response.Forbidden,`anda tidak punya hak untuk delete post dengan id:${id}`)

            const currentImgPath = findPost.media_path
            if(currentImgPath && fs.existsSync(currentImgPath)) {
                fs.unlinkSync(currentImgPath)
            }

            if(postData.media_url){
                await deleteImage(findPost.media_url)
            }

            const deletedPost = await Post.destroy({where:{id}})
            await Comment.destroy({where:{post_id:id}})
            
            deletedPost ? response.Success(response.Ok,`post dengan id:${id} berhasil didelete`)
                        : response.Fail(response.NotFound,`post dengan id:${id} tidak ditemukan`)

        } catch (error) {
            response.Fail(response.InternalServerError,'Internal Server Error')
        }
    }

}