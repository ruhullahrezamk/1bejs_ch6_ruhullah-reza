const {Account, Profile ,Post, Comment} = require('../models');
const Response = require('../utils/Response')

module.exports = {

    create : async (req,res)=>{
        const {message} = req.body
        const id = req.params.id
        const user = req.user
        const response = new Response(res)
        try {   
            
            const findPost = await Post.findOne({where:{id}})
            if(!findPost) return response.Fail(response.NotFound,`post dengan id:${id} tidak ditemukan`)

            const newComment = await Comment.create({message,post_id:id,account_id:user.id})
            if(newComment) response.Success(response.Created,'Comment berhasil ditambahkan')

        } catch (error) {
            response.Fail(response.InternalServerError,'Internal Server Error')
        }
    },
    
    getByPostId : async (req,res)=>{
        const id = req.params.id
        const response = new Response(res)
        try {       
            const options = {
                attributes:['id','message'],
                include: [{
                    model:Account,
                    attributes:['username'],
                    include: [{
                        model:Profile,
                        attributes:['nickname']
                    }], 
                }],
                where:{
                    post_id:id
                }
            }

            const findPost = await Post.findOne({where:{id}})
            if(!findPost) return response.Fail(response.NotFound,`post dengan id:${id} tidak ditemukan`)

            const allComment = await Comment.findAll(options)

            const allCommentMapped = allComment.map(eachComment=>{
                if(eachComment.dataValues.Account){
                    eachComment.dataValues.nickname = eachComment.dataValues.Account.dataValues.Profile.dataValues.nickname
                    eachComment.dataValues.username = eachComment.dataValues.Account.dataValues.username
                    delete eachComment.dataValues.Account
                    return eachComment
                }   
            })

            if(allComment) response.Success(response.Ok,allCommentMapped)

        } catch (error) {
            response.Fail(response.InternalServerError,'Internal Server Error')
        }
    },

    getByCurrentUser : async (req,res)=>{
        const id = req.params.id
        const response = new Response(res)
        const user = req.user
        try {       
            const options = {
                attributes:['id','message'],
                include: [{
                    model:Post,
                    attributes:['id','title'],
                }],
                where:{
                    account_id:user.id
                }
            }

            const allComment = await Comment.findAll(options)

            const allCommentMapped = allComment.map(eachComment=>{
                eachComment.dataValues.comment_id = eachComment.dataValues.id
                eachComment.dataValues.post_id = eachComment.dataValues.Post.id
                eachComment.dataValues.post_title = eachComment.dataValues.Post.title
                delete eachComment.dataValues.id
                delete eachComment.dataValues.Post
                return eachComment
            })

            if(allComment) response.Success(response.Ok,allCommentMapped)

        } catch (error) {
            response.Fail(response.InternalServerError,'Internal Server Error')
        }
    },

    deleteById : async (req,res)=>{
        const id = req.params.id
        const user = req.user
        const response = new Response(res)
        try {  
            const findComment = await Comment.findOne({where:{id}})
            if(!findComment) return response.Fail(response.NotFound,`comment dengan id:${id} tidak ditemukan`)

            const validUser = findComment.dataValues.account_id === user.id
            const isAdmin = user.isAdmin
            if(!validUser && !isAdmin) return response.Fail(response.Forbidden,`anda tidak punya hak untuk delete comment dengan id:${id}`)

            const deletedComment = await Comment.destroy({where:{id}})
            deletedComment ? response.Success(response.Ok,`comment dengan id:${id} berhasil didelete`)
                           : response.Fail(response.NotFound,`comment dengan id:${id} tidak ditemukan`) 
        } catch (error) {
            response.Fail(response.InternalServerError,'Internal Server Error')
        }
    },
}