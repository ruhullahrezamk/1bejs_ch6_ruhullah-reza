const {Comment, Account, Profile, Post} = require('../models')
const Response = require('../utils/Response')
const bcrypt = require('bcryptjs')
const jwt = require('jsonwebtoken')
const fs = require('fs')
const sendMail = require('../utils/mailer')
const { deleteImage } = require("../utils/cloudinary");
require('dotenv').config()

module.exports ={
    login :  async(req,res)=>{
        const response = new Response(res)
        const {email,username,password} = req.body
        try {
            const options = {
                where:{}
            }

            if(email) options.where.email = email
            if(username) options.where.username = username

            const userLogin = await Account.findOne(options)
            if(!userLogin || !password) return response.Fail(response.BadRequest,'username atau password salah')
            
            const userPassword = userLogin.dataValues.password
            const validPassword = await bcrypt.compare(password,userPassword)

            if(!validPassword) return response.Fail(response.BadRequest,'username atau password salah')

            const payload = {
                id:userLogin.dataValues.id,
                username:userLogin.dataValues.username,
                isAdmin: userLogin.dataValues.isAdmin
            }

            const token = jwt.sign(payload, process.env.jwt_secret,{expiresIn:'1d'})

            response.Success(response.Ok,token)

        } catch (error) {
            response.Fail(response.InternalServerError,'Internal Server Error')
        }
    },

    getOtp: async(req,res)=>{
        const response = new Response(res)
        const { email } = req.body;
        try {
            const otp = Math.random().toString(36).slice(-6).toUpperCase();
            const hashedOtp = bcrypt.hashSync(otp, +process.env.salt_round);

            const updatedAccount = await Account.update({otp: hashedOtp},{where: {email}});
            if (!updatedAccount[0]) return response.Fail(response.NotFound,'email tidak terdaftar')

            const otpMessage = `Kode OTP anda adalah : ${otp}, gunakan kode otp untuk mengubah password anda`;

            const emailResponse = await sendMail(email,'Kode OTP',otpMessage);

            response.Success(response.Ok,`Kode OTP dikirimkan ke email : ${emailResponse.accepted.join(',').split(',')}`)

        } catch (error) {  
            const FailedSendEmail =  error.responseCode >= 400
            if(FailedSendEmail) return response.Success(response.InternalServerError,'something went wrong with mailer, try again later')
           
            response.Fail(response.InternalServerError,'Internal Server Error')
        }
    },

    createAccount: async (req,res)=>{
        const {username, email, password} = req.body  
        const response = new Response(res)
        try {
            const duplicateEmail = await Account.findOne({where:{email}})
            if(duplicateEmail) return response.Fail(response.BadRequest,`${email} sudah digunakan`)

            const findUser = await Account.findOne({where:{username}})
            if(findUser) return response.Fail(response.BadRequest,`${username} sudah dibuat`)
            
            const salt = await bcrypt.genSalt(+process.env.salt_round)
            const hashedPassword = await bcrypt.hash(password,salt)

            const welcomeMessage = `selamat datang ${username}`
            const emailResponse = await sendMail(email,'Welcome To Social Media',welcomeMessage);
           
            const createdAccount = await Account.create({username,email,password : hashedPassword})
            const newProfile = await Profile.create({
                accountId : createdAccount.id,
                nickname: username
            })

            if(emailResponse && createdAccount && newProfile) response.Success(response.Created,`${username} berhasil dibuat, email berhasil dikirim`)
 
        } catch (error) { 
            const FailedSendEmail = error.responseCode >= 400
            if(FailedSendEmail) return response.Success(response.Created,`${username} berhasil dibuat, welcome email gagal dikirim`)
            
            response.Fail(response.InternalServerError,'Internal Server Error')
        }   
    },
    
    createAdmin: async (req,res)=>{
        const {username, password, email, isAdmin} = req.body
        const response = new Response(res)
        try {  
            const findUser = await Account.findOne({where:{username}})
            if(findUser) return response.Fail(response.BadRequest,`${username} sudah dibuat`)

            const salt = await bcrypt.genSalt(+process.env.salt_round)
            const hashedPassword = await bcrypt.hash(password,salt)

            const createdAccount = await Account.create({username,password : hashedPassword,email,isAdmin})
            const newProfile = await Profile.create({
                accountId : createdAccount.id,
                nickname: username
            })

            const welcomeMessage = `selamat datang ${username}`;
            const emailResponse = await sendMail(email,'Welcome To Social Media',welcomeMessage);

            if(emailResponse && createdAccount && newProfile) response.Success(response.Created,`${username} berhasil dibuat`)
        } catch (error) {
            const FailedSendEmail = error.responseCode >= 400
            if(FailedSendEmail) return response.Success(response.Created,`${username} berhasil dibuat, welcome email gagal dikirim`)
            
            response.Fail(response.InternalServerError,'Internal Server Error')
        }   
    },
    
    getUserByUsername : async (req,res)=>{
        const username = req.params.username
        const response = new Response(res)
        try {
        
            const userFound = await Account.findOne({include:Profile,where:{username}})
        
            if(!userFound) return response.Fail(response.NotFound,`${username} tidak ditemukan`)

            const data = {
                id:userFound.id,
                username:userFound.username,
                nickname:userFound.Profile.nickname,
                description:userFound.Profile.description,
                profile_picture_url:userFound.Profile.img_url,
            }

            return response.Success(response.Ok,data)

        } catch (error) {
            response.Fail(response.InternalServerError,'Internal Server Error')
        }
    },

    updateAccountPassword : async(req,res)=>{
        const response = new Response(res)
        const { email,otp,password } = req.body;

        if(!otp) return response.Fail(response.BadRequest,`otp tidak boleh kosong`)
        
        try {
            const salt = await bcrypt.genSalt(+process.env.salt_round)
            const hashedPassword = await bcrypt.hash(password,salt)

            const findAccount = await Account.findOne({where:{email}})
            if(!findAccount) return response.Fail(response.NotFound,`email:${email} tidak terdaftar`)

            if(!findAccount.otp) return response.Fail(response.BadRequest,`OTP tidak valid`)
            
            const validOtp = await bcrypt.compare(otp,findAccount.otp)
            if(!validOtp) return response.Fail(response.BadRequest,`OTP tidak valid`)

            const updatedAccount = await Account.update({password:hashedPassword,otp:null},{where : {email}})

            updatedAccount[0] ? response.Success(response.Ok,'password berhasil diubah')
                              : response.Fail(response.NotFound,`email:${email} tidak terdaftar`)

        } catch (error) { 
            response.Fail(response.InternalServerError,'Internal Server Error')
        }
    },

    updateProfile : async(req,res)=>{
        const { nickname, description, cloud_media_url } = req.body;
        const {username} = req.user
        const response = new Response(res)
        const img_url = req.file
        try {  
            const userFound = await Account.findOne({attributes:['id'],where:{username}})
            if(!userFound) return response.Fail(response.NotFound,`${username} tidak ditemukan`)
            
            const currentProfile = await Profile.findOne({where: {accountId: userFound.id}})
            if(!currentProfile) return response.Fail(response.NotFound,`${username} tidak ditemukan`)

            if(currentProfile.img_url) await deleteImage(currentProfile.img_url)
            
            const currentImgPath = currentProfile.img_path
            if(currentImgPath && fs.existsSync(currentImgPath)) fs.unlinkSync(currentImgPath)
            
            const updatedData = {nickname,description}
            if(img_url) {
                updatedData.img_url = cloud_media_url
                updatedData.img_path = req.file.path
            }

            const updatedProfile = await Profile.update(updatedData,{where : {accountId : userFound.id}})    
            if(updatedProfile) return response.Success(response.Ok,'profile berhasil diupdate')

        } catch (error) {
            response.Fail(response.InternalServerError,'Internal Server Error')
        }
    },

    deleteAccount: async(req,res)=>{
        const username = req.params.username;
        const response = new Response(res)
        try {
            const userFound = await Account.findOne({attributes:['id'],where:{username}})
            if(!userFound) return response.Fail(response.NotFound,`${username} tidak ditemukan`)

            const userProfile = await Profile.findOne({where : {accountId : userFound.id}})

            if(userProfile.img_url){
                await deleteImage(userProfile.img_url)
            }

            const currentImgPath = userProfile.img_path
            if(currentImgPath && fs.existsSync(currentImgPath)){
                fs.unlinkSync(currentImgPath)
            }    
            
            await Profile.destroy({where : {accountId : userFound.id}})
            await Post.destroy({where : {accountId : userFound.id}})
            await Comment.destroy({where : {account_id : userFound.id}})

            const deletedAccount = await Account.destroy({where:{username}})
 
            deletedAccount ? response.Success(response.Ok,`${username} berhasil dihapus`)
                           : response.Fail(response.NotFound,`${username} tidak ditemukan`)
           
        } catch (error) {
            response.Fail(response.InternalServerError,'Internal Server Error')
        }
    }
}