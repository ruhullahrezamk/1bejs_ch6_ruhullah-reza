require('dotenv').config()
const express = require('express')
const morgan = require('morgan')
const app = express()
const router = require('./Routes/index.route')
const errorHandler = require('./utils/errorHandler')

const cors = require('cors')
app.use(cors())
app.options('*', cors())

const swaggerJSON = require('./api-documentation/swagger-output.json');
const swaggerUI = require('swagger-ui-express');


app.use(morgan('tiny'));
app.use(express.json());
app.use(express.urlencoded({extended:true}));

app.use(`/api/v1.0`, router);
app.use('/docs', swaggerUI.serve, swaggerUI.setup(swaggerJSON));

app.get('/',(req,res)=>{res.redirect('https://documenter.getpostman.com/view/17275912/UyxqBhva')})

app.use(errorHandler);

module.exports = app;