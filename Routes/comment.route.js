const router = require('express').Router()
const comment = require('../Controller/comment.controller')
const commentValidator = require('../validation/comment.validation')

router.post('/:id',commentValidator,comment.create)
router.get('/',comment.getByCurrentUser)
router.get('/:id',comment.getByPostId)
router.delete('/:id',comment.deleteById)

module.exports = router