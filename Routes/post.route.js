const router = require('express').Router()
const post = require('../Controller/post.controller')
const postValidator = require('../validation/post.validation')
const uploadMedia = require('../utils/postMulter')
const cloudinary = require('../utils/cloudinary')

router.post('/',uploadMedia.single('media'),postValidator,cloudinary.postUpload,post.createPost)
router.get('/',post.getAllPost)
router.get('/:username',post.getAllPostByUsername)
router.get('/id/:id',post.getPostByid)
router.put('/id/:id',uploadMedia.single('media'),postValidator,cloudinary.postUpload,post.updatePostById)
router.delete('/id/:id',post.deletePostById)

module.exports = router