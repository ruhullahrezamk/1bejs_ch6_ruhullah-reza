const router = require('express').Router()
const user = require('../Controller/user.controller')
const userValidator = require('../validation/user.validation')
const profileValidator = require('../validation/profile.validation')
const emailValidator = require('../validation/email.validation')
const passwordValidator = require('../validation/password.validation')
const accountSanitize = require('../sanitize/account.sanitize')
const verify = require('../utils/verify')
const isAdmin = require('../utils/isAdmin')
const uploadProfile = require('../utils/profileMulter')
const cloudinary = require('../utils/cloudinary')

router.post('/login',accountSanitize,user.login)
router.post('/register',userValidator,emailValidator,accountSanitize,user.createAccount)
router.get('/:username',verify,user.getUserByUsername)
router.put('/profile',verify,uploadProfile.single('profile_pict'),profileValidator,cloudinary.profileUpload,user.updateProfile)

//reset password
router.post('/getOtp',accountSanitize,user.getOtp)
router.put('/',passwordValidator,user.updateAccountPassword)

//admin only
router.post('/admin',verify,isAdmin,userValidator,emailValidator,accountSanitize,user.createAdmin)
router.delete('/:username',verify,isAdmin,user.deleteAccount)

module.exports = router