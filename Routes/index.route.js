const router = require('express').Router()
const userRouter = require('./user.route')
const postRouter = require('./post.route')
const commentRouter = require('./comment.route')
const verify = require('../utils/verify')

router.use('/users',userRouter)
router.use('/posts',verify,postRouter)
router.use('/comments',verify,commentRouter)

module.exports = router