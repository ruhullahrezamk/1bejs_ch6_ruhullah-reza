module.exports = (req,res,next)=>{
    if(req.body.username) req.body.username = req.body.username.toLowerCase();
    if(req.body.email) req.body.email = req.body.email.toLowerCase();
    next();
}