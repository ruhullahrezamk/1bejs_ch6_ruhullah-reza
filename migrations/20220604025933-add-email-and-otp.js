'use strict';

module.exports = {
  async up (queryInterface, Sequelize) {
    await queryInterface.addColumn('Accounts', 'otp', Sequelize.STRING);
    await queryInterface.addColumn('Accounts', 'email', Sequelize.STRING);
  },

  async down (queryInterface, Sequelize) {
    await queryInterface.removeColumn('Accounts', 'otp');
    await queryInterface.removeColumn('Accounts', 'email');
  }
};

