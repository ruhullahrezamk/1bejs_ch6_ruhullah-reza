const app = require('../app')
const { sequelize } = require('../models');

const request = require('supertest');

describe('users GET', () => {
    test('get user and return status 200', async () => {
        const response = await request(app).get("/api/v1.0/users/username1");
        const {result} = response.body
        expect(response.statusCode).toBe(200)
        expect(result.username).toBe('username1')
        expect(result.nickname).toBe('nickname 1')
        expect(result.description).toBe('description 1')
    })

    test('get user and return status 404', async () => {
        const response = await request(app).get("/api/v1.0/users/username100");
        const {message} = response.body
        expect(response.statusCode).toBe(404)
        expect(message ).toBe('username100 tidak ditemukan')
    })
})

describe('users POST', () => {
    test('create user and return status 201', async () => {
        const newUser = {
            "username": "username4",
            "password": "password",
            "confirmPassword": "password"
        }
        const response = await request(app).post("/api/v1.0/users/")
                                           .send(newUser) 
                                           .set('Accept', 'application/json') 
        const {result} = response.body
        expect(response.statusCode).toBe(201)
        expect(result).toBe('username4 berhasil dibuat')
    })

    test('create user and return status 400', async () => {
        const newUser = {
            "username": "username4",
            "confirmPassword": "password"
        }
        const response = await request(app).post("/api/v1.0/users/")
                                           .send(newUser) 
                                           .set('Accept', 'application/json') 
        const {message} = response.body
        expect(response.statusCode).toBe(400)
        expect(message).toBe('username, password, dan confirmPassword tidak boleh kosong')
    })
})

describe('users PUT', () => {
    test('update password and return status 200', async () => {
        const newPassword = {
            "password": "passwordbaru",
            "confirmPassword": "passwordbaru"
          }
        const response = await request(app).put("/api/v1.0/users/username4")
                                           .send(newPassword) 
                                           .set('Accept', 'application/json') 
        const {result} = response.body
        expect(response.statusCode).toBe(200)
        expect(result).toBe('password berhasil diubah')
    })

    test('update password and return status 400', async () => {
        const newPassword = {
            "password": "passwordbaru1",
        }

        const response = await request(app).put("/api/v1.0/users/username4")
                                           .send(newPassword) 
                                           .set('Accept', 'application/json') 
        const {message} = response.body
        expect(response.statusCode).toBe(400)
        expect(message).toBe('password dan confirmPassword tidak boleh kosong')
    })
})

describe('users profile PUT', () => {
    test('update profile and return status 200', async () => {
        const newProfile = {
            "nickname": "nickname baru",
            "description": "deskripsi"
          }
        const response = await request(app).put("/api/v1.0/users/profile/username4")
                                           .send(newProfile) 
                                           .set('Accept', 'application/json') 
        const {result} = response.body
        expect(response.statusCode).toBe(200)
        expect(result).toBe('profile berhasil diupdate')
    })

    test('update profile and return status 404', async () => {
        const newProfile = {
            "nickname": "nickname baru",
            "description": "deskripsi"
          }

        const response = await request(app).put("/api/v1.0/users/profile/username30")
                                           .send(newProfile) 
                                           .set('Accept', 'application/json') 
        const {message} = response.body
        expect(response.statusCode).toBe(404)
        expect(message).toBe('username30 tidak ditemukan')
    })
})

describe('users DELETE', () => {
    test('delete user and return status 200', async () => {
        const response = await request(app).delete("/api/v1.0/users/username4")
                                           
        const {result} = response.body
        expect(response.statusCode).toBe(200)
        expect(result).toBe('username4 berhasil dihapus')
    })

    test('delete user and return status 404', async () => {
        const response = await request(app).delete("/api/v1.0/users/username30")
                                          
        const {message} = response.body
        expect(response.statusCode).toBe(404)
        expect(message).toBe('username30 tidak ditemukan')
    })
})

afterAll(async () => {
    await sequelize.queryInterface.bulkDelete('Accounts', null, { truncate: true, restartIdentity: true }) 
    await sequelize.queryInterface.bulkDelete('Profiles', null, { truncate: true, restartIdentity: true }) 
    await sequelize.queryInterface.bulkDelete('Posts', null, { truncate: true, restartIdentity: true })
})