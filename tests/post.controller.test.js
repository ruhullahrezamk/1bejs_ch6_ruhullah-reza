const app = require('../app')
const { sequelize } = require('../models');

const request = require('supertest');

describe('post GET', () => {
    test('get All post and return status 200', async () => {
        const response = await request(app).get("/api/v1.0/posts/");
        const {result} = response.body
        expect(response.statusCode).toBe(200)
    })

    test('get All post from specific username and return status 200', async () => {
        const response = await request(app).get("/api/v1.0/posts/username1");
        const {result} = response.body
        expect(response.statusCode).toBe(200)
    })

    test('get All post from specific username and return status 404', async () => {
        const response = await request(app).get("/api/v1.0/posts/username100");
        const {message} = response.body
        expect(response.statusCode).toBe(404)
        expect(message).toBe('user tidak ditemukan')
    })

    test('get post with id:2 and return status 200', async () => {
        const response = await request(app).get("/api/v1.0/posts/id/2");
        const {result} = response.body
        expect(response.statusCode).toBe(200)
        expect(result.title).toBe('title 2')
    })

    test('get post with id:200 and return status 404', async () => {
        const response = await request(app).get("/api/v1.0/posts/id/200");
        const {message} = response.body
        expect(response.statusCode).toBe(404)
        expect(message).toBe('post dengan id:200 tidak ditemukan')
    })
})

describe('post POST', () => {
    test('create post and return status 201', async () => {
        const newPost = {
            "title": "title 4",
            "content": "content 4"
        }
        const response = await request(app).post("/api/v1.0/posts/username1")
                                           .send(newPost)
                                           .set('Accept', 'application/json')
        const {result} = response.body
        expect(response.statusCode).toBe(201)
        expect(result).toBe('Post berhasil dibuat')
    })

    test('create post and return status 400', async () => {
        const newPost = {
            "content": "content 4"
        }
        const response = await request(app).post("/api/v1.0/posts/username1")
                                           .send(newPost)
                                           .set('Accept', 'application/json')
        const {message} = response.body
        expect(response.statusCode).toBe(400)
        expect(message).toBe('title dan content tidak boleh kosong')
    })

    test('create post and return status 404', async () => {
        const newPost = {
            "title": "title 4",
            "content": "content 4"
        }
        const response = await request(app).post("/api/v1.0/posts/username100")
                                           .send(newPost)
                                           .set('Accept', 'application/json')
        const {message} = response.body
        expect(response.statusCode).toBe(404)
        expect(message).toBe('user tidak ditemukan')
    })
})

describe('post PUT', () => {
    test('update post post and return status 200', async () => {
        const updatedPost = {
            "title": "title baru"
          }
        const response = await request(app).put("/api/v1.0/posts/id/3")
                                           .send(updatedPost)
                                           .set('Accept', 'application/json')
        const {result} = response.body
        expect(response.statusCode).toBe(200)
        expect(result).toBe('post dengan id:3 berhasil diupdate')
    })

    test('update post post and return status 400', async () => {
        const updatedPost = {
            "title": "title baru",
            "img_url": "img_url_baru"
          }
          
        const response = await request(app).put("/api/v1.0/posts/id/3")
                                           .send(updatedPost)
                                           .set('Accept', 'application/json')

        const {message} = response.body
        expect(response.statusCode).toBe(400)
        expect(message).toBe('title dan content tidak boleh kosong')
    })

    test('update post and return status 404', async () => {
        const updatedPost = {
            "title": "title 4",
            "content": "content 4",
            "img_url": "img_url4"
        }
        const response = await request(app).put("/api/v1.0/posts/id/33")
                                           .send(updatedPost)
                                           .set('Accept', 'application/json')
        const {message} = response.body
        expect(response.statusCode).toBe(404)
        expect(message).toBe('post dengan id:33 tidak ditemukan')
    })
})

describe('post DELETE', () => {
    test('delete post post and return status 200', async () => {
        const response = await request(app).delete("/api/v1.0/posts/id/3")

        const {result} = response.body
        expect(response.statusCode).toBe(200)
        expect(result).toBe('post dengan id:3 berhasil didelete')
    })

    test('delete post post and return status 404', async () => {
        const response = await request(app).delete("/api/v1.0/posts/id/33")

        const {message} = response.body
        expect(response.statusCode).toBe(404)
        expect(message).toBe('post dengan id:33 tidak ditemukan')
    })
})

