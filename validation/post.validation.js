const Response = require('../utils/Response')
module.exports = (req,res,next)=>{
    const response = new Response(res)
    const {title,content,img_url} = req.body
    
    if(!title) return response.Fail(response.BadRequest,'title tidak boleh kosong')
    if(!content) return response.Fail(response.BadRequest,'content tidak boleh kosong')

    const invalidPost = typeof title !== 'string' || typeof content !== 'string'
    const invalidImage = img_url && typeof img_url !== 'string'
    if(invalidPost || invalidImage) return response.Fail(response.BadRequest,'semua field harus bertipe string')

    next()
}