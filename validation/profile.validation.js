const Response = require('../utils/Response')
module.exports = (req,res,next)=>{
    const response = new Response(res)
    
    const { nickname, description } = req.body;

    if(!nickname) return response.Fail(response.BadRequest,'field nickname harus ada')
    if(!description) return response.Fail(response.BadRequest,'field description harus ada')

    if(description.length > 255) return response.Fail(response.BadRequest,'description terlalu panjang (maks 255 karakter)')
 
    next();
}