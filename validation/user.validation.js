const Response = require('../utils/Response')
module.exports = (req,res,next)=>{
    const response = new Response(res)
    const {username,email,password,confirmPassword} = req.body;
    
    if(!username) return response.Fail(response.BadRequest,'username tidak boleh kosong')

    if(!email) return response.Fail(response.BadRequest,'email tidak boleh kosong')

    if(!password) return response.Fail(response.BadRequest,'password tidak boleh kosong')

    if(!confirmPassword) return response.Fail(response.BadRequest,'confirmPassword tidak boleh kosong')

    if(typeof username !== 'string') return response.Fail(response.BadRequest,'semua field harus bertipe string')

    const validUsername = /^[0-9a-zA-Z]+$/;
    if(!username.match(validUsername)) return response.Fail(response.BadRequest,'username harus alphanumeric')

    const hasSpace = !password.match(/^\S*$/) && !confirmPassword.match(/^\S*$/)
    if(hasSpace) return response.Fail(response.BadRequest,'password tidak boleh mengandung spasi')

    if(password.length < 5) return response.Fail(response.BadRequest,'password minimal 5 karakter')

    if(password !== confirmPassword) return response.Fail(response.BadRequest,'password tidak cocok')

    next();
}