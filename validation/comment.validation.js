const Response = require('../utils/Response')
module.exports = (req,res,next)=>{
    const response = new Response(res);
    const {message} = req.body;

    if(!message) return response.Fail(response.BadRequest,'message tidak boleh kosong')

    if(message.length > 300) return response.Fail(response.BadRequest,'message maksimal 255 karakter')
    
    next();
}