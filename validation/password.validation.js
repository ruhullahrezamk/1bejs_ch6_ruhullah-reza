const Response = require('../utils/Response')
module.exports = (req,res,next)=>{
    const response = new Response(res);
    const {password,confirmPassword} = req.body;

    if(!password) return response.Fail(response.BadRequest,'password tidak boleh kosong')
    if(!confirmPassword) return response.Fail(response.BadRequest,'confirmPassword tidak boleh kosong')
   
    const hasSpace = !password.match(/^\S*$/) && !confirmPassword.match(/^\S*$/)
    if(hasSpace) return response.Fail(response.BadRequest,'password tidak boleh mengandung spasi')

    if(password.length < 5) return response.Fail(response.BadRequest,'password minimal 5 karakter')

    return
    
    if(password !== confirmPassword) return response.Fail(response.BadRequest,'password dan confirmPassword tidak cocok')

    next();
}