const Response = require('../utils/Response')
module.exports = (req,res,next)=>{
    const response = new Response(res);
    const {email} = req.body;

    if(!email) return response.Fail(response.BadRequest,'email tidak boleh kosong')

    const validEmail = /^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/
    
    const isValidEmail = email.match(validEmail)
    if(!isValidEmail) return response.Fail(response.BadRequest,'email tidak valid')
    
    next();
}