'use strict';
const {
  Model
} = require('sequelize');
module.exports = (sequelize, DataTypes) => {
  class Post extends Model {
    static associate(models) {
      Post.belongsTo(models.Account,{
        foreignKey:'accountId',
        as:'owner',
        onDelete: 'CASCADE', 
        hooks: true 
      })
      Post.hasMany(models.Comment,{
        foreignKey:'post_id',
      })
    }
  }
  Post.init({
    accountId: DataTypes.INTEGER,
    title: DataTypes.STRING,
    media_url: DataTypes.STRING,
    media_path: DataTypes.STRING,
    content: DataTypes.STRING
  }, {
    sequelize,
    modelName: 'Post',
  });
  return Post;
};