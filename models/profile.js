'use strict';
const {
  Model
} = require('sequelize');
module.exports = (sequelize, DataTypes) => {
  class Profile extends Model {
    static associate(models) {
      Profile.belongsTo(models.Account,{
        foreignKey:'accountId',
        onDelete: 'CASCADE', 
        hooks: true 
      })
    }
  }
  Profile.init({
    accountId: DataTypes.INTEGER,
    nickname: DataTypes.STRING,
    description: DataTypes.STRING,
    img_url: DataTypes.STRING,
    img_path: DataTypes.STRING,
  }, {
    sequelize,
    modelName: 'Profile',
  });
  return Profile;
};