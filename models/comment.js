'use strict';
const {
  Model
} = require('sequelize');
module.exports = (sequelize, DataTypes) => {
  class Comment extends Model {

    static associate(models) {
      Comment.belongsTo(models.Post,{
        foreignKey:'post_id',
        onDelete: 'CASCADE', 
        hooks: true 
      })
      Comment.belongsTo(models.Account,{
        foreignKey:'account_id',
        onDelete: 'CASCADE', 
        hooks: true 
      })
    }
  }
  Comment.init({
    message: DataTypes.STRING,
    post_id: DataTypes.INTEGER,
    account_id: DataTypes.INTEGER
  }, {
    sequelize,
    modelName: 'Comment',
  });
  return Comment;
};