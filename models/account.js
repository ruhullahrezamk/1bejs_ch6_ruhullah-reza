'use strict';
const {
  Model
} = require('sequelize');
module.exports = (sequelize, DataTypes) => {
  class Account extends Model {
    static associate(models) {
      Account.hasOne(models.Profile,{
        foreignKey:'accountId'
      })
      Account.hasMany(models.Post,{
        foreignKey:'accountId',
      })
      Account.hasMany(models.Comment,{
        foreignKey:'account_id',
      })
    }
  }
  Account.init({
    username: DataTypes.STRING,
    email: DataTypes.STRING,
    password: DataTypes.STRING,
    isAdmin: DataTypes.BOOLEAN,
    otp: DataTypes.STRING,
  }, {
    sequelize,
    modelName: 'Account',
  });

  return Account;
};